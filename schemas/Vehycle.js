const mongoose = require('mongoose');

let vehycleModal = new mongoose.Schema({
    vehycleType: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'VehycleType'
    },
    manufacturer: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    productionYear: {
        type: Number,
        required: true
    },
    state: {
        type: Boolean,
        default: true
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    registrationNo: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Vehycle', vehycleModal)