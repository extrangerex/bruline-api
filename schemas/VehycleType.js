const mongoose = require('mongoose');

let vehycleTypeModal = new mongoose.Schema({
    typeName: {
        type: String,
        required: true
    },
    icon: {
        type: String
    },
    tax: {
        type: Number,
        required: true
    }
})

module.exports = mongoose.model('VehycleType', vehycleTypeModal)