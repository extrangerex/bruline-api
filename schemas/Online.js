const mongoose = require('mongoose');

let onlineModal = new mongoose.Schema({
    vehycle: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Vehycle'
    },
    isOnRide: {
        type: Boolean,
        default: false
    },
    currentPos: [Number]
})

module.exports = mongoose.model('Online', onlineModal)