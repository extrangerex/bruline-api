const mongoose = require('mongoose');

let travelModel = new mongoose.Schema({
    rider: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    driver: {
        type: mongoose.Schema.Types.ObjectId
    },
    from: {
        type: [Number],
        required: true
    },
    to: {
        type: [Number],
        required: true
    },
    subtotal: {
        type: Number,
        required: true
    },
    status: {
        type: String
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    forTime: {
        type: Date
    },
    finished: {
        type: Boolean,
        default: false
    }
})