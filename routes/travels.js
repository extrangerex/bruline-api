const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const router = express.Router();


const User = require('../schemas/User')
const Vehycle = require('../schemas/Vehycle')
const Online = require('../schemas/Online')
const Travel = require('../schemas/Travel')
    // middlewares

const { verificaToken } = require('../middlewares/auth')

router.post('/', verificaToken, (req, res) => {
    let travelModal = new Travel({
        rider: req.body.travel.rider.id,
        from: req.body.travel.from,
        to: req.body.travel.to,

    })
});

module.exports = router