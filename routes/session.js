const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const router = express.Router();

const User = require('../schemas/User')
const Vehycle = require('../schemas/Vehycle')
const Online = require('../schemas/Online')

// FCM

const _fCloudm = require('fcm-node');

var fcm = new _fCloudm(process.env.fcm_serverKey);

// middlewares

const { verificaToken } = require('../middlewares/auth')

router.get('/', (req, res) => {
    res.send('tutto bene!');
});

router.post('/active_devices', (req, res) => {

    token = req.body.token;

    console.log(req.body);

    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: token,

        notification: {
            title: 'Title of your push notification',
            body: 'Body of your push notification'
        },


        "android": {
            "notification": {
                "sound": "default"
            }
        },

        data: { //you can send only notification or only data(or include both)
            my_key: 'my value',
            my_another_key: 'my another value'
        }
    };

    fcm.send(message, function(err, response) {
        if (err) {
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });
});

router.post('/driver', (req, res) => {
    let user = new User({
        name: req.body.name,
        type: 'driver',
        userName: req.body.userName,
        photoUrl: req.body.photoUrl,
        emailAddress: req.body.emailAddress,
        password: bcrypt.hashSync(req.body.password, 10),
        birthDate: Date.parse(req.body.birthDate)
    })

    user.save((err, userSaved) => {
        if (err) {
            return res.json({
                success: false,
                data: err
            });
        }

        return res.json({
            success: true,
            data: userSaved
        });
    })
});


router.post('/rider', (req, res) => {
    let user = new User({
        name: req.body.name,
        type: 'rider',
        userName: req.body.userName,
        emailAddress: req.body.emailAddress,
        password: bcrypt.hashSync(req.body.password, 10),
        birthDate: Date.parse(req.body.birthDate)
    })

    user.save((err, userSaved) => {
        if (err) {
            return res.json({
                success: false,
                data: err
            });
        }

        return res.json({
            success: true,
            data: userSaved
        });
    })
});

router.post('/login', (req, res) => {
    req.body = req.body || { emailAddress: null, password: null }

    console.log(req.body);

    if (req.body.emailAddress == null &&
        req.body.password == null) {
        return res.status(400).json({
            success: false,
            data: 'Some fields are missing at request'
        });
    }

    User.findOne({
        emailAddress: req.body.emailAddress
    }, (err, userFromDb) => {

        if (err) {
            return res.json({
                success: false,
                data: err
            });
        }

        if (userFromDb == null) return res.status(404).json({ success: false, data: 'User not found' });

        if (!bcrypt.compareSync(req.body.password, userFromDb.password)) {
            return res.json({
                success: false,
                data: 'You have entered an incorrect username or password'
            });
        }

        let token = jwt.sign(JSON.parse(JSON.stringify(userFromDb)), process.env.seed, { expiresIn: process.env.expiresIn });

        return res.json({
            success: true,
            data: token,
            usertype: userFromDb.type
        });


    })
});

router.post('/online', verificaToken, (req, res) => {

    Vehycle.findOne({ owner: req.body.decoded.id }).populate('owner').exec((err, userVehycle) => {
        if (err) {
            return res.json({
                success: false,
                data: err
            });
        }

        let onlineModal = new Online({
            vehycle: userVehycle,
            isOnRide: false,
            currentPos: [req.body.lat, req.body.lng]
        })

        onlineModal.save((err, isOnline) => {
            if (err) {
                return res.json({
                    success: false,
                    data: err
                });
            }

            return res.json({
                success: true,
                data: isOnline
            });
        })

    })
});

router.delete('/online', (req, res) => {
    Online.findOneAndRemove({ id: req.body.id }, (err, deletedOnlineSession) => {
        if (err) {
            return res.json({
                success: false,
                data: err
            });
        }


        return res.json({
            success: true,
            data: deletedOnlineSession
        });
    })
});

router.put('/online', verificaToken, (req, res) => {
    Online.findByIdAndUpdate({ id: req.body.id }, req.body.online, (err, updatedOnlineModal) => {
        if (err) {
            return res.json({
                success: false,
                data: err
            });
        }

        return res.json({
            success: true,
            data: updatedOnlineModal
        });
    })
});

router.get('/online', verificaToken, (req, res) => {

    req.body.params = req.body.params || {};

    Online.find(req.body.params, (err, onlineDrivers) => {
        if (err) {
            return res.json({
                success: false,
                data: err
            });
        }

        return res.json({
            success: true,
            data: onlineDrivers
        });
    })
});

router.post('/driver/vehycle', verificaToken, (req, res) => {
    User.findOne({ emailAddress: req.body.emailAddress }, (err, selectedDriver) => {
        if (err) {
            return res.json({
                success: false,
                data: err
            });
        }

        let vehycleModal = new Vehycle({
            vehycleType: req.body.vehycleType,
            manufacturer: req.body.manufacturer,
            model: req.body.model,
            productionYear: req.body.productionYear,
            state: req.body.state,
            owner: selectedDriver.id,
            registrationNo: req.body.registrationNo
        })

        vehycleModal.save((err, resultedSavedVehycle) => {
            if (err) {
                return res.json({
                    success: false,
                    data: err
                });
            }

            return res.json({
                success: true,
                data: resultedSavedVehycle
            });
        })
    })
});

module.exports = router