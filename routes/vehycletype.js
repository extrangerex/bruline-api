const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const router = express.Router();


const User = require('../schemas/User')
const Vehycle = require('../schemas/Vehycle')
const Online = require('../schemas/Online')

// middlewares

const { verificaToken } = require('../middlewares/auth')
const VehycleType = require('../schemas/VehycleType');

router.get('/type',(req,res) => {

    req.body.params = req.body.params || {};

    VehycleType.find(req.body.params, (err,vehycleTypeDB)=> {
        if(err){
            return res.json({
                success: false,
                data: err
            });
        }

        return  res.json({
            success: true,
            data: vehycleTypeDB
        });
    })
})

router.post('/type', (req, res) => {
    let vehycleModal = new VehycleType({
        typeName: req.body.typeName,
        icon: req.body.icon,
        tax: req.body.tax
    })

    vehycleModal.save((err, vehycleSaved) => {
        if(err){
            return res.json({
                success: false,
                data: err
            });
        }

        return  res.json({
            success: true,
            data: vehycleSaved
        });
    })
});

module.exports = router