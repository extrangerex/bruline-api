const { io } = require('../index');
const jwt = require('jsonwebtoken');

const Online = require('../schemas/Online');

io.use(function(socket, next) {
    if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(socket.handshake.token, process.env.seed, function(err, decoded) {
            if (err) return next(new Error('Authentication error'));
            socket.decoded = decoded;
            next();
        });
    } else {
        next(new Error('Authentication error'));
    }
}).on('connection', function(socket) {

    console.log('Nuevo Usuario');

    io.on('driver', function(callback) {
        callback('everything fine');
    })

    io.on('disconnect', function(socket) {
        console.log('disconnect');
    })
})