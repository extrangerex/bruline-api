const jwt = require('jsonwebtoken');

// Verificar Token
let verificaToken = async(req, res, next) => {
    let token = req.get('token');

    jwt.verify(token, process.env.seed, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                success: false,
                err
            });
        }
        req.body.decoded = decoded;
        next();
    })
}

module.exports = {
    verificaToken
}