const express = require('express');
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('./conf/config');
const webpush = require('web-push');

const app = express();

process.env.PORT = process.env.PORT || 3000;

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

// routes
app.use('/user', require('./routes/session'));
app.use('/vehycle', require('./routes/vehycletype'));

// middlewares
const { verificaToken } = require('./middlewares/auth')

mongoose.connect(config.db, { useNewUrlParser: true }, (err) => {
    if (err) {
        throw new Error(err);
    }
    console.log('Base de datos corriendo');
});

// web-push

webpush.setVapidDetails('malito:accounts@bruline.fr', process.env.web_push_publicVapidKey, process.env.web_push_privateVapidkey);

// subscribe routes

app.listen(process.env.PORT, () => {
    console.log(`Server started on ${process.env.PORT}`);
});